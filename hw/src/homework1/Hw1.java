package homework1;

import java.util.Random;
import java.util.Scanner;

public class Hw1 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Random rand = new Random();
        String name;
        System.out.println("Enter your name");
        name = scan.nextLine();
        int  randomNumber , userNumber;
        System.out.println("Let the game begin! " + name);
        randomNumber = rand.nextInt(101);


        do {
            System.out.println("Enter your number");
            userNumber = scan.nextInt();
            if( userNumber < randomNumber)
                System.out.println("Your number is too small. Please, try again.");
            else if (userNumber > randomNumber)
                System.out.println("Your number is too big. Please, try again.");
            else System.out.println("Congratulations: " + name + " - you win");
        } while (userNumber != randomNumber);
    }
}
