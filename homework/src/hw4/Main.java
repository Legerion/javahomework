package hw4;

public class Main {
    public static void main(String[] args) {
        String [] habitsPet1 = new String[2];
        habitsPet1[0] = "eat";
        habitsPet1[1] = "sleep";

        String [] habitsPet2 = new String[2];
        habitsPet2[0] = "gaf";
        habitsPet2[1] = "sleep";

        Pet petFirst = new Pet("cat", "Masha", 3, 85, habitsPet1);
        Pet petSecond = new Pet("dog", "Bobik", 4, 75, habitsPet2);
        Pet petThird = new Pet("dog", "Umka", 10, 45, habitsPet2);


        String weekday = "go to work";
        String dayoff = "relax";

        Human familyFatherFirst = new Human("Ivan", "Ivanov", 1975);
         familyFatherFirst.setIQ(95);

        Human familyMatherFirst = new Human("Maria", "Ivanova", 1978);
        familyMatherFirst.setIQ(89);

        for (int i = 1; i < 6; i++) {
            familyFatherFirst.addScheduleTask(i,weekday);
            familyMatherFirst.addScheduleTask(i, dayoff);
        }

        familyFatherFirst.addScheduleTask(6, dayoff);
        familyFatherFirst.addScheduleTask(0, weekday);
        familyMatherFirst.addScheduleTask(6, dayoff);
        familyMatherFirst.addScheduleTask(0, weekday);

        Human familyChildFirst = new Human();
        familyChildFirst.setName("Bogdan");
        familyChildFirst.setSurname("Ivanov");
        familyChildFirst.setYear(2000);
        familyChildFirst.setIQ(91);
        familyChildFirst.setFather(familyFatherFirst);
        familyChildFirst.setMother(familyMatherFirst);
        familyChildFirst.setPet(petSecond);

        Human familyChildSecond = new Human("Alex", "Ivanov", 1998);
        familyChildSecond.setIQ(82);
        familyChildSecond.setFather(familyFatherFirst);
        familyChildSecond.setMother(familyMatherFirst);
        familyChildSecond.setPet(petThird);

        Human[] chidren1 = new Human[1];
        chidren1[0] = familyChildFirst;

        Family family1 = new Family(familyMatherFirst, familyFatherFirst, chidren1, petFirst);
        System.out.println(family1.toString());
        family1.addChild(familyChildSecond);
        System.out.println(family1.toString());
        family1.deleteChild(1);
        System.out.println(family1.toString());
        family1.deleteChild(0);
        System.out.println(family1.toString());

    }

}
