package hw4;

import java.util.Arrays;

public class Family {
    private Human father;
    private Human mother;
    private Human[] children;
    private final Pet pet;

    public Family( Human father,Human mother, Human[] children, Pet pet) {
        this.father = father;
        this.mother = mother;
        this.children = children;
        this.pet = pet;
    }

    public Human[] getChildren() {
        return this.children;
    }

    public void addChild(Human child) {
        this.children = Arrays.copyOf(children, children.length + 1);
        this.children[children.length - 1] = child;
    }

    public void deleteChild(int childIndex) {
        if(children.length > 1 && childIndex != this.children.length - 1) {
            for (int i = childIndex; i < this.children.length; i++) {
                this.children[childIndex] = this.children[childIndex + 1];
            }
            this.children = Arrays.copyOf(this.children, this.children.length - 1);
        } else this.children = new Human[0];
    }

    public int countFamily() {
        return children.length + 2;
    }

    @Override
    public String toString() {
        return "Family {" + "\n" +
                " amount= " + countFamily() + "\n" +
                " mother= " + mother + "\n" +
                " father= " + father + "\n" +
                " children= "  + Arrays.toString(children) + "\n" +
                " pet= " + pet + "\n" +
                '}';
    }

}
