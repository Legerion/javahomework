package hw4;

public enum DaysOfWeek {
    Sun("Sunday"),
    Mon("Monday"),
    Tue("Tuesday"),
    Wed("Wednesday"),
    Thu("Thursday"),
    Fri("Friday"),
    Sat("Saturday");

    private final String day;

    DaysOfWeek(String day) {
        this.day = day;
    }

    public String getDay() {
        return day;
    }

}
