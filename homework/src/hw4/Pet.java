package hw4;

import java.util.Arrays;

public class Pet {
    private String species;
    private String nickname;
    private int age;
    private int tricklevel;
    private String[] habits;

    public Pet() {
        this.species = "";
        this.nickname = this.species;
        this.age = 0;
        this.tricklevel = 0;
        this.habits = new String[0];
    }

    public Pet(String species, String name) {
        this.species = species;
        this.nickname = name;
        this.age = 0;
        this.tricklevel = 0;
        this.habits = new String[0];
    }

    public Pet(String species, String name, int age, int tricklevel, String[] habits) {
        this.species = species;
        this.nickname = name;
        this.age = age;
        this.tricklevel = tricklevel;
        this.habits = habits;
    }

    @Override
    public String toString() {
        return species + "{" +
                "nickname='" + nickname + '\'' +
                ", age= " + age +
                ", tricklevel= " + tricklevel +
                ", habits=" + Arrays.toString(habits) +
                '}';
    }

    public static void eat() {
        System.out.println("Я кушаю!");
    }

    public static void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }

    public void respond() {
        System.out.printf("Привет, хозяин. Я - %s. Я соскучился!\n", this.nickname);
    }

    public String getSpecies() {
        return this.species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public void changeName(String name) {
        this.nickname = name;
    }

    public String getName() {
        return this.nickname;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getAge() {
        return this.age;
    }

    public void setTrickLevel(int x) {
        if (Utils.intMatch(x, 0, 100)) this.tricklevel = x;
        else throw new IllegalArgumentException("tricklevel should be 0 - 100 integer");
    }

    public int getTrickLevel() {
        return this.tricklevel;
    }

    public void addHabit(String h) {
        this.habits = Arrays.copyOf(habits, habits.length + 1);
        habits[habits.length - 1] = h;
    }

    public String[] getHabits() {
        return this.habits;
    }


}
