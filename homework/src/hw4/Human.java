package hw4;

import java.util.Arrays;

public class Human {
    private String name;
    private String surname;
    private int year;
    private Human mother;
    private Human father;
    private int iq;
    private Pet pet;
    private final String[][] schedule;

    @Override
    public String toString(){
        return "Human" + "{ " +
                "name: " + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", mother=" + mother +
                ", father=" + father +
                ", pet=" + pet.getSpecies() +
                ":" + "nickname='" + pet.getName() + '\'' +
                "age=" + pet.getAge() +
                "tricklevel=" + pet.getTrickLevel() +
                "habits=" + Arrays.toString(pet.getHabits()) +
                ", schedule=" + Utils.arrToStr(schedule) +
                '}';
    }


    public Human() {
        this.name = "";
        this.surname = "";
        this.year = 0;
        this.iq = 0;
        this.pet = new Pet();
        this.schedule = Utils.createSchedule();
    }

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.pet = new Pet();
        this.schedule = Utils.createSchedule();
    }

    public Human(String name, String surname, int year, int iq, Pet pet, Human mother, Human father,String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.pet = pet;
        this.mother = mother;
        this.father = father;
        this.schedule = schedule;
    }

    public void setName (String n) {

        this.name = n;
    }
    public String getName() {

        return this.name;
    }

    public void setSurname (String sn) {

        this.surname = sn;
    }

    public String getSurname() {
        return this.surname;
    }

    public void setYear (int year) {

        this.year = year;
    }

    public int getYear() {

        return this.year;
    }

    public void setMother(Human m) {

        this.mother = m;
    }
    public Human getMother() {

        return this.mother;
    }

    public void setFather(Human f) {

        this.father = f;
    }

    public Human getFather() {

        return this.father;
    }

    public void setIQ(int x) {
        if(Utils.intMatch(x, 0, 100)) this.iq = x;
        else throw new IllegalArgumentException("iq should be 0 - 100 integer");
    }

    public int getIQ() {
        return this.iq;
    }

    public void setPet (Pet p) {
        this.pet = p;
    }

    public void greetPet() {
        System.out.printf("Привет, %s \n", this.pet.getName());
    }

    public void describePet() {
        String trickLvl = this.pet.getTrickLevel() > 50? "очень хитрый" : "почти хитрый";
        System.out.printf("У меня есть %s, ему %d, он %s", this.pet.getSpecies(), this.pet.getAge(), trickLvl);
    }

    public String[][] getSchedule() {
        return this.schedule;
    }

    public void addScheduleTask (int dayIndex, String task) {
        this.schedule[dayIndex][1] = task;
    }

    public void removeScheduleTask (int dayIndex) {
        this.schedule[dayIndex][1] = "";
    }

}
