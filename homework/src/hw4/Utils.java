package hw4;

import java.util.Arrays;

public class Utils {
    public static String[][] createSchedule() {
        String[][] schedule = new String [7][2];
        int c = 0;
        for (DaysOfWeek day: DaysOfWeek.values()) {
            schedule[c][0] = day.getDay();
            schedule[c++][1] = "";
        }
        return schedule;
    }

    public static boolean intMatch(int x ,int initial, int bound) {
        if (x < initial) return false;
        return x <= bound;
    }

    public static String arrToStr (String[][] a) {
        String s = new String("[");
        for (int i = 0; i < a.length; i++) {
            if (i != a.length - 1) s+= Arrays.toString(a[i]) + ",";
            else s+= Arrays.toString(a[i]) + "]";
        }
        return s;
    }

}
