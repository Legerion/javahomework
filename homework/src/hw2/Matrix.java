package hw2;

import java.util.Random;
import java.util.Scanner;

public class Matrix {
    public static short strToShort (String s) {
        return Short.parseShort(s);
    }

    private static boolean isShort (String s) {
        try {
            short v = strToShort(s);

            if(v > 5) throw new NumberFormatException();
            if(v < 1) throw new NumberFormatException();
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private static short expectDigit(Scanner in, short bound) {
        do {
            String s = in.next();
            if(isShort(s)) return strToShort(s);
            System.out.printf("You wrote: `%s`.This is not a number. Any row and column has to be a digit 1 - %d" +
                    " number." + " Please try again: ", s, bound);
        } while (true);
    }

    public static short randomShort(short b) {
        short rand =  (short) new Random().nextInt(b + 1);
        if(rand == 0) rand = 1;
        return rand;
    }

    private static char[][] generateArr(short size) {
        char[][] arr = new char[size + 1][size +1];

        for (short i = 0; i <= size; i++ ) {
            for (short j = 0; j <= size; j++) {
                arr[0][j] = (char) (j +'0');
                if (j == 0) arr[i][j] = (char) (i + '0');
                else arr[i][j] = '-';
            }
        }
        return arr;
    }

    private static char[][] changeArr(char[][]arr , short x, short y, short tRow, short tCol) {
        if(x == 0 && y == 0) arr[x][y] = '0';
        else if(x == tRow && y == tCol) arr[x][y] = 'X';
        else arr[x][y] = '*';
        return arr;
    }

    private static void printMatrix(char[][] arr) {
        for (char[] i : arr) {
            StringBuilder line = new StringBuilder(" ");

            for(char j : i) {
                line.append(j).append(" | ");
            }
            System.out.println(line);
        }
    }

    public static void clearScreen() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

    public static void main(String[] args) {
        short size = 5;
        short tCol = randomShort(size);
        short tRow = randomShort(size);
        short x = 0;
        short y = 0;
        char[][] area = generateArr(size);
        Scanner scan = new Scanner(System.in);

        System.out.println("All set. Get ready to rumble!");

        while (true) {
            printMatrix(area);
            if(x == tRow && y == tCol) {
                System.out.println("You have won!");
                break;
            }
            System.out.print("Please enter the column number: ");
            y = expectDigit(scan, size);
            System.out.print("Please enter the row number: ");
            x = expectDigit(scan, size);

            changeArr(area, x, y, tRow, tCol);
            clearScreen();
        }
    }

}
