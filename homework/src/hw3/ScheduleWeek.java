package hw3;

import java.util.Scanner;

public class ScheduleWeek {

    public static void printTasks(String[][] schedule, int dayIndex) {
        System.out.printf("Your tasks for %s: %s \n", schedule[dayIndex][0], schedule[dayIndex][1]);
    }

    public static void printInformation () {
        System.out.println("Please, input day of the week: ");
    }

    public static void printError (){
        System.out.println("Sorry, I don't understand you, please try again.");
    }

    public static String removeLastSpace(String s) {
        String result = "";
        char lastChar = s.charAt(s.length() - 1);
        int bound = lastChar == (' ') ? s.length() - 1 : s.length();

        for (int i = 0; i < bound; i++) result+= s.charAt(i);
        return result;
    }

    public static void main(String[] args) {
        String[][] schedule = new String[7][2];

        schedule[0][0] = "Sunday";
        schedule[1][0] = "Monday";
        schedule[2][0] = "Tuesday";
        schedule[3][0] = "Wednesday";
        schedule[4][0] = "Thursday";
        schedule[5][0] = "Friday";
        schedule[6][0] = "Saturday";

        schedule[0][1] = "Do home work";
        schedule[1][1] = "Go to courses; watch a film";
        schedule[2][1] = "Go to gym";
        schedule[3][1] = "Go shopping";
        schedule[4][1] = "Meeting with parents";
        schedule[5][1] = "Visit to the doctor";
        schedule[6][1] = "Meeting with friends";

        String selectedDay = "";

        while (!selectedDay.equals("exit")) {
            int dayIndex = -2;

            while (dayIndex == -2) {
                printInformation();
                String input = new Scanner(System.in).nextLine().toLowerCase();
                selectedDay = removeLastSpace(input);

                switch (selectedDay) {
                    case "sunday":
                        dayIndex = 0;
                        break;
                    case "monday":
                        dayIndex = 1;
                        break;
                    case "tuesday":
                        dayIndex = 2;
                        break;
                    case "wednesday":
                        dayIndex = 3;
                        break;
                    case "thursday":
                        dayIndex = 4;
                        break;
                    case "friday":
                        dayIndex = 5;
                        break;
                    case "saturday":
                        dayIndex = 6;
                        break;
                    case "exit":
                        dayIndex = -1;
                        break;
                    default:
                        dayIndex = -2;
                        break;
                }
                if (dayIndex == -2) printError();
            }
            if (dayIndex != -1) printTasks(schedule, dayIndex);
        }
    }
}
